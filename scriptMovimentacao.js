document.addEventListener("DOMContentLoaded",()=>{
    let url = "listarMovimentacao.php";
    let resultado = document.querySelector("#listaMovimentacao");

    getData(url)
        .then(function (response) {
            // manipula o sucesso da requisição
            let tabela="";
            //monta as linhas da tabela
            response.data.forEach(element => {
                let vencimentoOriginal = element.vencimento;
                let partesVencimento = vencimentoOriginal.split("-");
                let vencimentoFormatado = partesVencimento[2] + "/" + partesVencimento[1] + "/" + partesVencimento[0];

                let entradaOriginal = element.entrada;
                let partesEntrada = entradaOriginal.split("-");
                let entradaFormatada = partesEntrada[2] + "/" + partesEntrada[1] + "/" + partesEntrada[0];

                let btnPago = "";

                if (element.pago == 0 ) {
                    btnPago += `<button type="button" class="tableButton" onclick="pagaItem(${element.id})" style="color:green">Pagar</button>`;
                } else {
                    btnPago += `<button type="button" class="tableButton" onclick="despagaItem(${element.id})" style="color:blue">Desfazer</button>`;
                };

                tabela+=`<tr data-id="${element.id}">
                    <td>${element.id}</td>
                    <td>${element.nome}</td>
                    <td>${element.conta}</td>
                    <td>${element.valor.toLocaleString('pt-br', {minimumFractionDigits: 2})}</td>
                    <td>${vencimentoFormatado}</td>
                    <td>${entradaFormatada}</td>
                    <td>${btnPago}</td>
                    <td><button type="button" class="tableButton" onclick="deletaItem(${element.id})" style="color:red">Deletar</button></td>
                </tr>`;
            });
            resultado.innerHTML=tabela;
        })
        .catch(function (error) {
            // manipula erros da requisição
            console.error(error);
        })
        .finally(function () {
            // sempre será executado
        });

        window.deletaItem = function(id){
            let url = "deletaItem.php";
            deleteRecord(url, {data: {id: id}})
            .then((res)=>{
                if(res.data.codigo=="1"){
                    Swal.fire({
                        title: 'Sucesso',
                        text: res.data.texto,
                        icon: 'success',
                        showConfirmButton: false
                    });
                    setTimeout(() => {
                        location.reload();    
                    }, 2000);
                }
                else {
                    Swal.fire({
                        title: 'Erro',
                        text: res.data.texto,
                        icon: 'error',
                        confirmButtonText: 'OK',
                        confirmButtonColor: '#111011'
                    })
                }
            })
        }

        window.pagaItem = function(id){
            let url = "pagaItem.php";
            modifyRecord(url, {id: id})
            .then((res)=>{
                if(res.data.codigo=="1"){
                    Swal.fire({
                        title: 'Sucesso',
                        text: res.data.texto,
                        icon: 'success',
                        showConfirmButton: false
                    });
                    setTimeout(() => {
                        location.reload();    
                    }, 2000);
                }
                else {
                    Swal.fire({
                        title: 'Erro',
                        text: res.data.texto,
                        icon: 'error',
                        confirmButtonText: 'OK',
                        confirmButtonColor: '#111011'
                    })
                }
            })
        }
        window.despagaItem = function(id){
            let url = "despagaItem.php";
            modifyRecord(url, {id: id})
            .then((res)=>{
                if(res.data.codigo=="1"){
                    Swal.fire({
                        title: 'Sucesso',
                        text: res.data.texto,
                        icon: 'success',
                        showConfirmButton: false
                    });
                    setTimeout(() => {
                        location.reload();    
                    }, 2000);
                }
                else {
                    Swal.fire({
                        title: 'Erro',
                        text: res.data.texto,
                        icon: 'error',
                        confirmButtonText: 'OK',
                        confirmButtonColor: '#111011'
                    })
                }
            })
        }
})