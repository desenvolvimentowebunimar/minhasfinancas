function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

function addToDisplay(value) {
    const activeTab = document.querySelector(".tabcontent[style*='display: block;']");
    const displayId = activeTab.id.replace('calculadora-', 'display-');
    document.getElementById(displayId).value += value;
}

function clearDisplayNormal() {
    document.getElementById('display-normal').value = '';
}

function calculateResultNormal() {
    try {
        document.getElementById('display-normal').value = eval(document.getElementById('display-normal').value);
    } catch (error) {
        document.getElementById('resultado-normal').textContent = 'Erro';
    }
}

function calcularJurosCompostos() {
    const principal = parseFloat(document.getElementById("principal-juros").value);
    const taxa = parseFloat(document.getElementById("taxa-juros").value) / 100;
    const tempo = parseFloat(document.getElementById("tempo-juros").value);
    const tipoJuros = document.getElementById("tipo-juros").value; // Pega o tipo de juros selecionado

    if (tipoJuros === "1") {
        const montante = principal * Math.pow(1 + taxa, tempo);
        const juros = montante - principal;
        const resultadoElement = document.getElementById("resultado-juros");
        resultadoElement.innerHTML = `
            Montante após ${tempo} anos: R$ ${montante.toFixed(2)}<br>
            Juros totais: R$ ${juros.toFixed(2)}
        `;
    } else if (tipoJuros === "2") {
        const montante = principal * Math.pow(1 + taxa / 12, tempo * 12); // Taxa mensal e tempo em meses
        const juros = montante - principal;
        const resultadoElement = document.getElementById("resultado-juros");
        resultadoElement.innerHTML = `
            Montante após ${tempo} anos: R$ ${montante.toFixed(2)}<br>
            Juros totais: R$ ${juros.toFixed(2)}
        `;
    }
}

function calcularInvestimentos() {
    const principal = parseFloat(document.getElementById("principal-investimentos").value);
    const taxa = parseFloat(document.getElementById("taxa-investimentos").value) / 100; // Converter para decimal
    const contribuicoes = parseFloat(document.getElementById("contribuicoes-investimentos").value);
    const tempo = parseFloat(document.getElementById("tempo-investimentos").value);

    let montante = principal;

    for (let i = 0; i < tempo; i++) {
        montante += contribuicoes;
        montante *= (1 + taxa);
    }

    const resultadoElement = document.getElementById("resultado-investimentos");
    resultadoElement.innerHTML = `
        Valor futuro do investimento após ${tempo} anos: R$ ${montante.toFixed(2)}
    `;
}


function myFunction(x) {
    x.classList.toggle("change");
    document.querySelector(".subMenuItens").classList.toggle("mostrar");
}

document.addEventListener("DOMContentLoaded", () => {
    function myFunction(x) {
        x.classList.toggle("change");
        document.querySelector(".subMenuItens").classList.toggle("mostrar");
    }
});

