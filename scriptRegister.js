document.addEventListener("DOMContentLoaded",()=>{
    function envia(){
        let url="gravaUsuario.php";
        let form = document.querySelector("#fmrAdicionarUsuario");
        postForm(url,form).then(res=>{
            if(res.data.codigo=="1"){
                Swal.fire({
                    title: 'Sucesso',
                    text: res.data.texto,
                    icon: 'success',
                    showConfirmButton: false
                    });
                    setTimeout(() => {
                        window.location.href = '../index.html';
                    }, 2000);
            }
            else{
                Swal.fire({
                    title: 'Erro',
                    text: res.data.texto,
                    icon: 'error',
                    confirmButtonText: 'OK',
                    confirmButtonColor: '#111011'
                    })
                document.querySelector('#confirmarEmailRegister').value = '';
                document.querySelector('#passwordRegister').value = '';
                document.querySelector('#confirmarPasswordRegister').value = '';
            }
        })
    }
    
    document.querySelector("#btnAdicionarUsuario").addEventListener("click",function(){
        
        let email = document.querySelector("#emailRegister").value
        let emailConfirm = document.querySelector("#confirmarEmailRegister").value
        let senha = document.querySelector("#passwordRegister").value
        let senhaConfirm = document.querySelector("#confirmarPasswordRegister").value

        if ( email != emailConfirm ) {
            window.alert("E-mails não conferem.");
        } else if ( senha != senhaConfirm ) {
            window.alert("Senhas não conferem.");
        } else {
            envia();
            //window.alert("Cadastro efetuado.");
        }
    });

});