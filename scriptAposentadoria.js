document.addEventListener("DOMContentLoaded", () => {
    function myFunction(x) {
        x.classList.toggle("change");
        document.querySelector(".subMenuItens").classList.toggle("mostrar");
    }
    document.querySelector("#btnAposentadoria").addEventListener("click",function(){
        var idadeAtual = document.getElementById('idadeAtual').value;
        var salario = document.getElementById('salario').value;
    var tempoContribuicao = document.getElementById('tempoContribuicao').value;

    // Verifica se todos os campos foram preenchidos
    if (idadeAtual && salario && tempoContribuicao) {
        // Calcula a aposentadoria (10% do salário pelo tempo de contribuição)
        var aposentadoria = (salario * 0.1) * tempoContribuicao;
        
        // Exibe o resultado
        document.querySelector('#resultado').innerHTML = "O valor da aposentadoria é R$ " + aposentadoria.toFixed(2);
    } else {
        alert('Por favor, preencha todos os campos.');
    }
});
});

