<?php 
session_start();
if(!$_SESSION["logado"]){
    header("Location: login.html");
    exit;
}

require("conexao.php");

date_default_timezone_set("America/Sao_Paulo");

$idUsuario = $_SESSION["id"];

$sql="SELECT SUM(valor) as total
      FROM financas.itens 
      WHERE fk_user = :par_user 
      AND pago = :par_pago
      AND vencimento <= :par_vencimento";

$stmt=$conn->prepare($sql);
$stmt->execute(array(
    ":par_user"=>$idUsuario,
    ":par_pago"=>1,
    ":par_vencimento"=>date("Y-m-d")
));
$total = $stmt->fetchAll(PDO::FETCH_OBJ);

$sql="SELECT SUM(saldo) as credito
      FROM financas.contas
      WHERE fk_user = :par_user AND tipo = :par_tipo";
$stmt=$conn->prepare($sql);
$stmt->execute(array(
                ":par_user"=>$idUsuario,
                ":par_tipo"=>1));
$credito = $stmt->fetchAll(PDO::FETCH_OBJ);

$sql="SELECT SUM(valor) as dividas
      FROM financas.itens
      WHERE fk_user = :par_user 
      AND pago = :par_pago";
$stmt=$conn->prepare($sql);
$stmt->execute(array(
                ":par_user"=>$idUsuario,
                ":par_pago"=>0
            ));
$divida = $stmt->fetchAll(PDO::FETCH_OBJ);

$sql = "SELECT itens.id, itens.nome, itens.conta, itens.valor, itens.vencimento, itens.entrada 
        FROM itens
        WHERE itens.fk_user = :par_user
        ORDER BY itens.id DESC
        LIMIT 4";

$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ":par_user" => $idUsuario
));

$movimentacao = $stmt->fetchAll(PDO::FETCH_OBJ);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Minhas Finanças: Principal</title>
    <link rel="stylesheet" href="../styles/stylePrincipal.css">
    <link rel="stylesheet" href="../style.css">
    <script src="../scripts.js"></script>
</head>
<body>
    <header>
        <div id="divisao-topo">
            <div class="menuEsquerda">
                <div class="menuButton" onclick="myFunction(this)">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                </div>
                <a href="principal.php" class="inicio">Minhas Finanças</a>
                <div class="subMenuItens">
                    <a href="./contas/contas.php">Contas</a>
                    <a href="./metas/metas.php">Metas</a>
                    <a href="./investimentos/investimentos.php">Investimentos</a>
                    <a href="./dividas/dividas.php">Dívidas</a>
                    <a href="calculadoras.php">Calculadoras</a>
                </div>
            </div>
            <ul class="opcoesPerfil">
                <li><a href="sair.php">Sair</a></li>
            </ul>
        </div>
    </header>
    <main>
        <section id="principal">
            <div>
                <h2>Bem-vindo, <?php echo($_SESSION["usuario"]);?></h2>
                <div id="saldoAtual" class="saldoAtual">
                    <span>Saldo atual</span>
                    <h3>R$ <?php echo number_format($total[0]->total,2,",",".");?></h3>
                </div>
                <div id="creditoDisponivel" class="creditoDisponivel">
                    <span>Credito disponível:</span>
                    <span>R$ <?php echo number_format($credito[0]->credito,2,",",".");?></span>
                </div>
                <div id="creditoDisponivel" class="creditoDisponivel">
                    <span>Dívidas em aberto</span>
                    <span>R$ <?php echo number_format($divida[0]->dividas,2,",",".");?></span>
                </div>
                <a href="adicionarItem.php"><button>Adicionar Item</button></a>
            </div>
        </section>
        <hr>
        <section class="movimentacoesPrincipal">
            <p>Últimas movimentações</p>
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NOME</th>
                            <th>CONTA</th>
                            <th>VALOR</th>
                            <th>VENCIMENTO</th>
                            <th>ENTRADA</th>
                        </tr>
                    </thead>
                    <tbody id="listaMovimentacao">
                        <?php foreach ($movimentacao as $item): ?>
                            <tr>
                                <td><?php echo $item->id; ?></td>
                                <td><?php echo $item->nome; ?></td>
                                <td><?php echo $item->conta; ?></td>
                                <td><?php echo number_format($item->valor,2,",","."); ?></td>
                                <td><?php echo implode("/",array_reverse(explode("-",$item->vencimento))); ?></td>
                                <td><?php echo implode("/",array_reverse(explode("-",$item->entrada))); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <a href="movimentacao.php"><button>Ver mais</button></a>
        </section>
    </main>
    <footer>
        <div>
            Minhas Finanças© 2023
        </div>
    </footer>
</body>
</html>