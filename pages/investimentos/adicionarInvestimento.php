<?php 
session_start();
if(!$_SESSION["logado"]){
    header("Location: ../login.html");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Minhas Finanças: Investimentos</title>
    <link rel="stylesheet" href="../../styles/stylePrincipal.css">
    <link rel="stylesheet" href="../../styles/styleAdicionarItem.css">
    <link rel="stylesheet" href="../../styles/styleLoginRegister.css">
    <link rel="stylesheet" href="../../style.css">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.32/dist/sweetalert2.all.min.js"></script>
    <link href=" https://cdn.jsdelivr.net/npm/sweetalert2@11.7.32/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="../../scripts.js"></script>
    <script src="../../lib.js"></script>
    <script src="./scriptGravarInvestimento.js"></script>
</head>
<body>
<header>
        <div id="divisao-topo">
            <div class="menuEsquerda">
            <div class="menuButton" onclick="myFunction(this)">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                </div>
                <a href="../principal.php" class="inicio">Minhas Finanças</a>
                    <div class="subMenuItens">
                      <a href="../contas/contas.php">Contas</a>
                      <a href="../metas/metas.php">Metas</a>
                      <a href="investimentos.php">Investimentos</a>
                      <a href="../dividas/dividas.php">Dívidas</a>
                      <a href="../calculadoras.php">Calculadoras</a>
                    </div>
            </div>
            <ul class="opcoesPerfil">
                <li><a href="../sair.php">Sair</a></li>
            </ul>
        </div>
    </header>
    <main>
        <section id="principal">
            <div>
                <h2>Adicionar investimento</h2>
                <form name="fmrAdicionarInvestimento" id="fmrAdicionarInvestimento" >
                    <div>
                        <label for="nomeInvestimento">Identificador</label>
                        <input type="text" name="nomeInvestimento" id="nomeInvestimento" class="input">
                    </div>
                    <div>
                        <label for="valorInvestimento">Valor inicial</label>
                        <input type="number" name="valorInvestimento" id="valorInvestimento" class="mediumInput">
                    </div>
                    <button type="button" name="btnAdicionarInvestimento" id="btnAdicionarInvestimento">Adicionar</button>
                </form>
                <a href="investimentos.php"><button>Cancelar</button></a>
            </div>
        </section>
    </main>
    <footer>
        <div>
            Minhas Finanças© 2023
        </div>
    </footer>
</body>
</html>