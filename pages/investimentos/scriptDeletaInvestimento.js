document.addEventListener("DOMContentLoaded", () => {
    window.deletaInvestimento = function(id){
        let url = "deletaInvestimento.php";
        deleteRecord(url, {data: {id: id}})
        .then((res)=>{
            if(res.data.codigo=="1"){
                Swal.fire({
                    title: 'Sucesso',
                    text: res.data.texto,
                    icon: 'success',
                    showConfirmButton: false
                });
                setTimeout(() => {
                    location.reload();    
                }, 2000);
            }
            else {
                Swal.fire({
                    title: 'Erro',
                    text: res.data.texto,
                    icon: 'error',
                    confirmButtonText: 'OK',
                    confirmButtonColor: '#111011'
                })
            }
        })
    }
});