<?php
session_start();
ini_set("display_errors",1);
require("../conexao.php");

$idUsuario = $_SESSION["id"];

$x=file_get_contents('php://input');

$x=json_decode($x);

$nome=$x->nomeInvestimento;
$valor=$x->valorInvestimento;

if ($nome === "") {
    $msg=array("codigo"=>0,"texto"=>"Identificador do investimento não informado.");
} else {
    $sql="insert into financas.investimentos(
                                    nome,
                                    valor,
                                    valor_atual,
                                    fk_user
                                    )
        values(
                :par_nome,
                :par_valor,
                :par_atual,
                :par_user
        );";
    $stmt = $conn->prepare($sql);
    $dados=array(":par_nome"=>$nome,
                ":par_valor"=>$valor,
                ":par_atual"=>$valor,
                ":par_user"=>$idUsuario ,
                );
    $result=$stmt->execute($dados);

    if($result){
        $msg=array("codigo"=>1,"texto"=>"Registro inserido com sucesso.");
    }
    else{
        $msg=array("codigo"=>0,"texto"=>"Erro ao inserir.");
    }
}
header('Content-Type: application/json; charset=utf-8');
echo(json_encode($msg));