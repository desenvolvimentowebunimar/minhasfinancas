<?php

session_start();
ini_set("display_errors",1);
require("../conexao.php");

$idUsuario = $_SESSION["id"];

$sql = "SELECT id, nome, valor, valor_atual
        FROM financas.investimentos 
        WHERE fk_user = :id_user";

$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ":id_user" => $idUsuario
));

$result = $stmt->fetchAll(PDO::FETCH_OBJ);

header('Content-Type: application/json; charset=utf-8');
echo(json_encode($result));
