<?php
session_start();
ini_set("display_errors", 1);
require("../conexao.php");
$data = file_get_contents("php://input");
$x = json_decode($data);

if (!empty($x->id) && !empty($x->valor)) {
    $id = $x->id;
    $atual = $x->valor;
    $idUsuario = $_SESSION["id"];

    $sql = "UPDATE financas.investimentos
            SET valor_atual = :par_atual
            WHERE id = :id AND fk_user = :id_user";

    $stmt = $conn->prepare($sql);
    $result = $stmt->execute(array(
        ":id" => $id,
        ":id_user" => $idUsuario,
        ":par_atual" => $atual
    ));

    if ($result) {
        $msg=array("codigo" =>1, "texto" => "Alterado com sucesso.");
    } else {
        $msg=array("codigo" => 0, "texto" => "Item não encontrado ou você não tem permissão para excluí-la.");
    }
} else {
    $msg=array("codigo" => 0, "texto" => "Valor não informado.");
}
header('Content-Type: application/json; charset=utf-8');
echo(json_encode($msg));
