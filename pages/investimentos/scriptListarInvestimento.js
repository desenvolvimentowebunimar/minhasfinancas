document.addEventListener("DOMContentLoaded",()=>{

    function listaConta(){
        let url = "listarInvestimento.php";
        let resultado = document.querySelector("#listaContas");

        getData(url)
            .then(function (response) {
                // manipula o sucesso da requisição
                let tabela="";
                //monta as linhas da tabela
                response.data.forEach(element => {
                    let rendimento = (element.valor_atual - element.valor) / element.valor * 100;

                    tabela+=`<tr>
                        <td>${element.id}</td>
                        <td>${element.nome}</td>
                        <td>${element.valor.toLocaleString('pt-br', {minimumFractionDigits: 2})}</td>
                        <td>${element.valor_atual.toLocaleString('pt-br', {minimumFractionDigits: 2})}</td>
                        <td>${rendimento.toLocaleString('pt-br', {minimumFractionDigits: 3})}%</td>
                        <td><button type="button" class="tableButton" onclick="abreAlteraInvestimento(${element.id},'${element.nome}')" style="color:blue">Alterar</button></td>
                        <td><button type="button" class="tableButton" onclick="deletaInvestimento(${element.id})" style="color:red">Deletar</button></td>
                        </tr>`;
                });
                resultado.innerHTML=tabela;
            })
            .catch(function (error) {
                // manipula erros da requisição
                console.error(error);
            })
            .finally(function () {
                // sempre será executado
            });
    }

    listaConta();
})