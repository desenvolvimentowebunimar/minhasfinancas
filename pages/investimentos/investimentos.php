<?php 
session_start();
if(!$_SESSION["logado"]){
    header("Location: ../login.html");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Minhas Finanças: Investimentos</title>
    <link rel="stylesheet" href="../../styles/stylePrincipal.css">
    <link rel="stylesheet" href="../../style.css">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.32/dist/sweetalert2.all.min.js"></script>
    <link href=" https://cdn.jsdelivr.net/npm/sweetalert2@11.7.32/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="../../scripts.js"></script>
    <script src="../../lib.js"></script>
    <script src="./scriptListarInvestimento.js"></script>
    <script src="./scriptAlteraInvestimento.js"></script>
    <script src="./scriptDeletaInvestimento.js"></script>
</head>
<body>
<header>
        <div id="divisao-topo">
            <div class="menuEsquerda">
            <div class="menuButton" onclick="myFunction(this)">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                </div>
                <a href="../principal.php" class="inicio">Minhas Finanças</a>
                    <div class="subMenuItens">
                      <a href="../contas/contas.php">Contas</a>
                      <a href="../metas/metas.php">Metas</a>
                      <a href="investimentos.php">Investimentos</a>
                      <a href="../dividas/dividas.php">Dívidas</a>
                      <a href="../calculadoras.php">Calculadoras</a>
                      <a href="#">Aprenda</a>
                    </div>
            </div>
            <ul class="opcoesPerfil">
                <li><a href="">Conta</a></li>
                <li><a href="../sair.php">Sair</a></li>
            </ul>
        </div>
    </header>
    <main>
        <section id="principal">
            <div>
                <h2>Investimentos</h2>
                <a href="adicionarInvestimento.php"><button>Adicionar</button></a>
            </div>
        </section>
        <hr>
        <section class="contasCadastradas">
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NOME</th>
                            <th>VALOR INICIAL</th>
                            <th>VALOR ATUAL</th>
                            <th>RENDIMENTO</th>
                        </tr>
                    </thead>
                    <tbody id="listaContas"></tbody>
                </table>
            </div>
        </section>
        <div>
            <form id="fmrAlteraInvestimento" class="fmrAlteraInvestimento subMenuItens" >
                <div id="areaInvestimento"></div>
                <label for="novoValor">Insira o valor:</label>
                <input type="number" name="novoValor" id="novoValor" class="input">
                <button type="button" id="btnAlteraValor" class="button">Confirmar</button>
                <button id="btnCancelar">Cancelar</button>
            </form>
        </div>
    </main>
    <footer>
        <div>
            Minhas Finanças© 2023
        </div>
    </footer>
</body>
</html>