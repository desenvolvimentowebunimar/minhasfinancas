<?php
session_start();
if(!$_SESSION["logado"]){
    header("Location: login.html");
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Minhas Finanças: Movimentações</title>
    <link rel="stylesheet" href="../styles/stylePrincipal.css">
    <link rel="stylesheet" href="../styles/styleAdicionarItem.css">
    <link rel="stylesheet" href="../styles/styleLoginRegister.css">
    <link rel="stylesheet" href="../style.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.32/dist/sweetalert2.all.min.js"></script>
    <link href=" https://cdn.jsdelivr.net/npm/sweetalert2@11.7.32/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="../scripts.js"></script>
    <script src="../scriptMovimentacao.js"></script>
    <script src="../lib.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</head>
<body>
<header>
        <div id="divisao-topo">
            <div class="menuEsquerda">
            <div class="menuButton" onclick="myFunction(this)">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                </div>
                <a href="principal.php" class="inicio">Minhas Finanças</a>
                    <div class="subMenuItens">
                      <a href="./contas/contas.php">Contas</a>
                      <a href="./metas/metas.php">Metas</a>
                      <a href="./investimentos/investimentos.php">Investimentos</a>
                      <a href="./dividas/dividas.php">Dívidas</a>
                      <a href="calculadoras.php">Calculadoras</a>
                    </div>
            </div>
            <ul class="opcoesPerfil">
                <li><a href="sair.php">Sair</a></li>
            </ul>
        </div>
    </header>
    <main>
        <section id="principal">
            <div>
                <h2>Movimentações</h2>
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NOME</th>
                            <th>CONTA</th>
                            <th>VALOR</th>
                            <th>VENCIMENTO</th>
                            <th>ENTRADA</th>
                        </tr>
                    </thead>
                    <tbody id="listaMovimentacao"></tbody>
                </table>
            </div>
        </section>
    </main>
    <footer>
        <div>
            Minhas Finanças© 2023
        </div>
    </footer>
</body>
</html>