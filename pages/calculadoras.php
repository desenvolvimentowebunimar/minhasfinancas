<?php 
session_start();
if(!$_SESSION["logado"]){
    header("Location: login.html");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Minhas Finanças: Calculadoras</title>
    <link rel="stylesheet" href="../styles/stylePrincipal.css">
    <link rel="stylesheet" href="../styles/styleCalculadora.css">
    <link rel="stylesheet" href="../style.css">
    <script src="../scripts.js"></script>
    <script src="../scriptAposentadoria.js"></script>
</head>
<body>
<header>
        <div id="divisao-topo">
            <div class="menuEsquerda">
            <div class="menuButton" onclick="myFunction(this)">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                </div>
                <a href="principal.php" class="inicio">Minhas Finanças</a>
                    <div class="subMenuItens">
                      <a href="./contas/contas.php">Contas</a>
                      <a href="./metas/metas.php">Metas</a>
                      <a href="./investimentos/investimentos.php">Investimentos</a>
                      <a href="./dividas/dividas.php">Dívidas</a>
                      <a href="calculadoras.php">Calculadoras</a>
                    </div>
            </div>
            <ul class="opcoesPerfil">
                <li><a href="sair.php">Sair</a></li>
            </ul>
        </div>
    </header>
    <main>
        <section id="principal">
                        
            <h2>Calculadoras</h2>
            <p>Escolha sua Calculadora</p>

            <div class="tab">
                <button class="tablinks" onclick="openCity(event, 'calculadora-normal')">Calculadora Normal</button>
                <button class="tablinks" onclick="openCity(event, 'calculadora-juros')">Calculadora de Juros</button>
                <button class="tablinks" onclick="openCity(event, 'calculadora-investimentos')">Calculadora de Investimentos</button>
                <button class="tablinks" onclick="openCity(event, 'calculadora-aposentadoria')">Calculadora de Aposentadoria</button>
            </div>

            <div id="calculadora-normal" class="tabcontent">
                <div class="calculator">
                    <input type="text" id="display-normal" readonly class="input">
                        <div class="buttons">
                            <button onclick="addToDisplay('7')">7</button>
                            <button onclick="addToDisplay('8')">8</button>
                            <button onclick="addToDisplay('9')">9</button>
                            <button onclick="addToDisplay('+')">+</button>
                            <button onclick="addToDisplay('4')">4</button>
                            <button onclick="addToDisplay('5')">5</button>
                            <button onclick="addToDisplay('6')">6</button>
                            <button onclick="addToDisplay('-')">-</button>
                            <button onclick="addToDisplay('1')">1</button>
                            <button onclick="addToDisplay('2')">2</button>
                            <button onclick="addToDisplay('3')">3</button>
                            <button onclick="addToDisplay('*')">*</button>
                            <button onclick="addToDisplay('.')">.</button>
                            <button onclick="addToDisplay('0')">0</button>
                            <button onclick="calculateResultNormal()">=</button>
                            <button onclick="clearDisplayNormal()">C</button>
                            <button onclick="addToDisplay('/')">/</button>
                        </div>
                    <div id="resultado-normal"></div>
                </div>
            </div>

            <div id="calculadora-juros" class="tabcontent">
                <div class="calculator">
                    <h2>Calculadora de Juros Compostos</h2>
                    <label for="principal-juros">Principal (R$):</label>
                    <input type="number" id="principal-juros" step="0.01" placeholder="Digite o valor principal" class="input">
                    <label for="taxa-juros">Taxa de Juros (%):</label>
                    <input type="number" id="taxa-juros" step="0.01" placeholder="Digite a taxa de juros " class="input">
                    <select id="tipo-juros">
                        <option value="1">Anual</option>
                        <option value="2">Mensal</option>
                    </select>
                    <label for="tempo-juros">Tempo:</label>
                    <input type="number" id="tempo-juros" step="0.01" placeholder="Digite o tempo" class="input">
                    <button onclick="calcularJurosCompostos()">Calcular</button>
                    <div id="resultado-juros"></div>
                </div>
            </div>

            <div id="calculadora-investimentos" class="tabcontent">
            <div class="calculator">
                    <h2>Calculadora de Investimentos</h2>
                    <label for="principal-investimentos">Investimento Inicial (R$):</label>
                    <input type="number" id="principal-investimentos" step="0.01" placeholder="Digite o valor inicial" class="input">
                    <label for="taxa-investimentos">Taxa de Juros Anual (%):</label>
                    <input type="number" id="taxa-investimentos" step="0.01" placeholder="Digite a taxa de juros anual" class="input">
                    <label for="contribuicoes-investimentos">Contribuições Mensais (R$):</label>
                    <input type="number" id="contribuicoes-investimentos" step="0.01" placeholder="Digite o valor das contribuições mensais" class="input">
                    <label for="tempo-investimentos">Tempo (Anos):</label>
                    <input type="number" id="tempo-investimentos" step="0.01" placeholder="Digite o tempo em anos" class="input">
                    <button onclick="calcularInvestimentos()">Calcular</button>
                    <div id="resultado-investimentos"></div>
                </div>


                    </div>
                    <div id="calculadora-aposentadoria" class="tabcontent">
                    <div class="calculator">
                    <h1>Calculadora de Aposentadoria</h1>

                <form id="calculadoraForm">
                <label for="idadeAtual">Idade Atual:</label>
                <input type="number" id="idadeAtual" required>

                <label for="salario">Salário:</label>
                <input type="number" id="salario" required>

                <label for="tempoContribuicao">Tempo de Contribuição:</label>
                <input type="number" id="tempoContribuicao" required>

                <button type="button" id="btnAposentadoria">Calcular</button>
                 </form>
        
                <div id="resultado"></div>

        
                </div>
            </div>


            </div>
        </section>
    </main>
    <footer>
        <div>
            Minhas Finanças© 2023
        </div>
    </footer>

</body>
</html>