<?php

session_start();
ini_set("display_errors",1);
require("conexao.php");

$idUsuario = $_SESSION["id"];

$sql = "SELECT itens.id, itens.nome, itens.conta, itens.valor, itens.vencimento, itens.entrada, itens.pago 
        FROM itens
        WHERE itens.fk_user = :par_user
        ORDER BY itens.id DESC";

$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ":par_user" => $idUsuario
));

$result = $stmt->fetchAll(PDO::FETCH_OBJ);

header('Content-Type: application/json; charset=utf-8');
echo(json_encode($result));
