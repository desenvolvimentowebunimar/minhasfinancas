document.addEventListener("DOMContentLoaded",()=>{
    window.pagaDivida = function(id){
        let url = "pagaDivida.php";
        modifyRecord(url, {id: id})
        .then((res)=>{
            if(res.data.codigo=="1"){
                Swal.fire({
                    title: 'Sucesso',
                    text: res.data.texto,
                    icon: 'success',
                    showConfirmButton: false
                });
                setTimeout(() => {
                    location.reload();    
                }, 2000);
            }
            else {
                Swal.fire({
                    title: 'Erro',
                    text: res.data.texto,
                    icon: 'error',
                    confirmButtonText: 'OK',
                    confirmButtonColor: '#111011'
                })
            }
        })
    }
    window.despagaDivida = function(id){
        let url = "despagaDivida.php";
        modifyRecord(url, {id: id})
        .then((res)=>{
            if(res.data.codigo=="1"){
                Swal.fire({
                    title: 'Sucesso',
                    text: res.data.texto,
                    icon: 'success',
                    showConfirmButton: false
                });
                setTimeout(() => {
                    location.reload();    
                }, 2000);
            }
            else {
                Swal.fire({
                    title: 'Erro',
                    text: res.data.texto,
                    icon: 'error',
                    confirmButtonText: 'OK',
                    confirmButtonColor: '#111011'
                })
            }
        })
    }
});