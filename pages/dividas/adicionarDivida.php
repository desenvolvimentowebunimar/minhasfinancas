<?php 
session_start();
if(!$_SESSION["logado"]){
    header("Location: ../login.html");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Minhas Finanças: Dívidas</title>
    <link rel="stylesheet" href="../../styles/stylePrincipal.css">
    <link rel="stylesheet" href="../../styles/styleAdicionarItem.css">
    <link rel="stylesheet" href="../../styles/styleLoginRegister.css">
    <link rel="stylesheet" href="../../style.css">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.32/dist/sweetalert2.all.min.js"></script>
    <link href=" https://cdn.jsdelivr.net/npm/sweetalert2@11.7.32/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="../../scripts.js"></script>
    <script src="../../lib.js"></script>
    <script src="./scriptGravarDivida.js"></script>
</head>
<body>
<header>
        <div id="divisao-topo">
            <div class="menuEsquerda">
            <div class="menuButton" onclick="myFunction(this)">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                </div>
                <a href="../principal.php" class="inicio">Minhas Finanças</a>
                <div class="subMenuItens">
                      <a href="../contas/contas.php">Contas</a>
                      <a href="../metas/metas.php">Metas</a>
                      <a href="../investimentos/investimentos.php">Investimentos</a>
                      <a href="dividas.php">Dívidas</a>
                      <a href="../calculadoras.php">Calculadoras</a>
                    </div>
            </div>
            <ul class="opcoesPerfil">
                <li><a href="../sair.php">Sair</a></li>
            </ul>
        </div>
    </header>
    <main>
        <section id="principal">
            <div>
                <h2>Adicionar dívida</h2>
                <form name="fmrAdicionarDivida" id="fmrAdicionarDivida">
                    <div>
                        <label for="nomeDivida">Identificador</label>
                        <input type="text" name="nomeDivida" id="nomeDivida" class="input">
                    </div>
                    <div>
                        <label for="valorDivida">Valor total</label>
                        <input type="number" name="valorDivida" id="valorDivida" class="mediumInput">
                    </div>
                    <div>
                        <label for="vencimentoDivida">Vencimento inicial</label>
                        <input type="date" name="vencimentoDivida" id="vencimentoDivida" class="mediumInput">
                    </div>
                    <div>
                        <label for="parcelasDivida">Parcelas</label>
                        <input type="number" name="parcelasDivida" id="parcelasDivida" class="mediumInput" value="1">
                    </div>
                    <button type="button" name="btnAdicionarDivida" id="btnAdicionarDivida">Adicionar</button>
                </form>
                <a href="dividas.php"><button>Cancelar</button></a>
            </div>
        </section>
    </main>
    <footer>
        <div>
            Minhas Finanças© 2023
        </div>
    </footer>
</body>
</html>