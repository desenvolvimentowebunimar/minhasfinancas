document.addEventListener("DOMContentLoaded",()=>{
    function envia(){
        let url="gravaDivida.php";
        let form = document.querySelector("#fmrAdicionarDivida");
        postForm(url,form).then(res=>{
            if(res.data.codigo=="1"){
                Swal.fire({
                    title: 'Sucesso',
                    text: res.data.texto,
                    icon: 'success',
                    showConfirmButton: false
                    });
                    setTimeout(() => {
                        window.location.href = 'dividas.php';
                    }, 2000);
            }
            else{
                Swal.fire({
                    title: 'Erro',
                    text: res.data.texto,
                    icon: 'error',
                    confirmButtonText: 'OK',
                    confirmButtonColor: '#111011'
                    })
            }
        })
    }

    document.querySelector("#btnAdicionarDivida").addEventListener("click",function(){
        envia();
    });

})