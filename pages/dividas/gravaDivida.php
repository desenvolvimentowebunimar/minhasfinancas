<?php
session_start();
ini_set("display_errors",1);
require("../conexao.php");

$idUsuario = $_SESSION["id"];

$x=file_get_contents('php://input');

$x=json_decode($x);

$nomeDivida=$x->nomeDivida;
$valor=$x->valorDivida;
$vencimento=$x->vencimentoDivida;
$vencimento=implode("-",array_reverse(explode("/",$vencimento)));
$parcelas=$x->parcelasDivida;
$valor=$valor/$parcelas;

if ($nomeDivida === "") {
    $msg=array("codigo"=>0,"texto"=>"Identificador da divida não informado.");
} else {
    for ($i = 1; $i <= $parcelas; $i++) {
        $sql="insert into financas.dividas(
                                        nome,
                                        valor,
                                        vencimento,
                                        parcelas,
                                        fk_user
                                        )
            values(
                    :par_nome,
                    :par_valor,
                    :par_vencimento,
                    :par_parcelas,
                    :par_user
            );";
        $stmt = $conn->prepare($sql);
        $dados=array(":par_nome"=>$nomeDivida,
                    ":par_valor"=>$valor,
                    ":par_vencimento"=>$vencimento,
                    ":par_parcelas"=>$i,
                    ":par_user"=>$idUsuario ,
                    );
        $result=$stmt->execute($dados);

        $vencimentoObj = date_create($vencimento);
        date_add($vencimentoObj, date_interval_create_from_date_string('1 month'));
        $vencimento = date_format($vencimentoObj, 'Y-m-d');
        }

    if($result){
        $msg=array("codigo"=>1,"texto"=>"Registro inserido com sucesso.");
    }
    else{
        $msg=array("codigo"=>0,"texto"=>"Erro ao inserir.");
    }
}
header('Content-Type: application/json; charset=utf-8');
echo(json_encode($msg));