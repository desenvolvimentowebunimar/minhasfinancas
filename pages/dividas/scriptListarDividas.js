document.addEventListener("DOMContentLoaded",()=>{

    function listaConta(){
        let url = "listarDivida.php";
        let resultado = document.querySelector("#listaContas");

        getData(url)
            .then(function (response) {
                // manipula o sucesso da requisição
                let tabela="";
                //monta as linhas da tabela
                response.data.forEach(element => {
                    
                    let vencimentoOriginal = element.vencimento;
                    let partesVencimento = vencimentoOriginal.split("-");
                    let vencimentoFormatado = partesVencimento[2] + "/" + partesVencimento[1] + "/" + partesVencimento[0];

                    let btnPago = "";

                    if (element.pago == 0) {
                        btnPago += `<button type="button" class="tableButton" onclick="pagaDivida(${element.id})" style="color:green">Pagar</button>`;
                    } else {
                        btnPago += `<button type="button" class="tableButton" onclick="despagaDivida(${element.id})" style="color:blue">Desfazer</button>`;
                    }

                    tabela+=`<tr>
                        <td>${element.id}</td>
                        <td>${element.nome}</td>
                        <td>${element.valor.toLocaleString('pt-br', {minimumFractionDigits: 2})}</td>
                        <td>${vencimentoFormatado}</td>
                        <td>${element.parcelas}</td>
                        <td>${btnPago}</td>
                        <td><button type="button" class="tableButton" onclick="deletaDivida(${element.id})" style="color:red">Deletar</button></td>    
                    </tr>`;
                });
                resultado.innerHTML=tabela;
            })
            .catch(function (error) {
                // manipula erros da requisição
                console.error(error);
            })
            .finally(function () {
                // sempre será executado
            });
    }
    listaConta();
})