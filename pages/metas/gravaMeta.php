<?php
session_start();
ini_set("display_errors",1);
require("../conexao.php");

$idUsuario = $_SESSION["id"];

$x=file_get_contents('php://input');

$x=json_decode($x);

$nomeMeta=$x->nomeMeta;
$valor=$x->valorMeta;
$vencimento=$x->vencimentoMeta;
$vencimento=implode("-",array_reverse(explode("/",$vencimento)));

if ($nomeMeta === "") {
    $msg=array("codigo"=>0,"texto"=>"Identificador da meta não informado.");
} else {
    $sql="insert into financas.metas(
                                    nome,
                                    valor,
                                    vencimento,
                                    fk_user
                                    )
        values(
                :par_nome,
                :par_valor,
                :par_vencimento,
                :par_user
        );";
    $stmt = $conn->prepare($sql);
    $dados=array(":par_nome"=>$nomeMeta,
                ":par_valor"=>$valor,
                ":par_vencimento"=>$vencimento,
                ":par_user"=>$idUsuario ,
                );
    $result=$stmt->execute($dados);

    if($result){
        $msg=array("codigo"=>1,"texto"=>"Registro inserido com sucesso.");
    }
    else{
        $msg=array("codigo"=>0,"texto"=>"Erro ao inserir.");
    }
}
header('Content-Type: application/json; charset=utf-8');
echo(json_encode($msg));