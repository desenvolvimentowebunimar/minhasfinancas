<?php

session_start();
ini_set("display_errors",1);
require("../conexao.php");

$idUsuario = $_SESSION["id"];

$sql = "SELECT id, nome, valor, vencimento, coalesce(guardado,0) as guardado 
        FROM financas.metas 
        WHERE fk_user = :id_user
        ORDER BY vencimento ASC";

$stmt = $conn->prepare($sql);
$stmt->execute(array(
    ":id_user" => $idUsuario
));

$result = $stmt->fetchAll(PDO::FETCH_OBJ);

header('Content-Type: application/json; charset=utf-8');
echo(json_encode($result));
