document.addEventListener("DOMContentLoaded",()=>{
    window.abreGuardaMeta = function(id, nome) {
        document.querySelector('#fmrGuardaMeta').classList.toggle('mostrar');
        
        let meta = `<input type="text" id="codigo" name="${id}" value="${id}" class="input" readonly style="display:none"></input><br><h3>${nome}</h3>`;
        document.querySelector('#areaMeta').innerHTML = meta;
    }

    document.querySelector('#btnAlteraValor').addEventListener('click',function(){
        let url="guardaMeta.php";
        let id = document.querySelector("#codigo").value;
        let valor = document.querySelector("#novoValor").value;

        let form = {id: id, valor:valor};

        console.log(form);
        modifyRecord(url,form).then(res=>{
            if(res.data.codigo=="1"){
                Swal.fire({
                    title: 'Sucesso',
                    text: res.data.texto,
                    icon: 'success',
                    confirmButtonText: 'OK',
                    showConfirmButton: false
                    });
                    setTimeout(() => {
                        location.reload();    
                    }, 2000);
                    
            }
            else{
                Swal.fire({
                    title: 'Erro',
                    text: res.data.texto,
                    icon: 'error',
                    confirmButtonText: 'OK'
                    })
            }
        })
    });
});