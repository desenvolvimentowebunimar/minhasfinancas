<?php 
session_start();
if(!$_SESSION["logado"]){
    header("Location: ../login.html");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Minhas Finanças: Metas</title>
    <link rel="stylesheet" href="../../styles/stylePrincipal.css">
    <link rel="stylesheet" href="../../styles/styleAdicionarItem.css">
    <link rel="stylesheet" href="../../styles/styleLoginRegister.css">
    <link rel="stylesheet" href="../../style.css">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.32/dist/sweetalert2.all.min.js"></script>
    <link href=" https://cdn.jsdelivr.net/npm/sweetalert2@11.7.32/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="../../scripts.js"></script>
    <script src="../../lib.js"></script>
    <script src="./scriptGravarMeta.js"></script>
</head>
<body>
<header>
        <div id="divisao-topo">
            <div class="menuEsquerda">
            <div class="menuButton" onclick="myFunction(this)">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                </div>
                <a href="../principal.php" class="inicio">Minhas Finanças</a>
                    <div class="subMenuItens">
                      <a href="../contas/contas.php">Contas</a>
                      <a href="metas.php">Metas</a>
                      <a href="../investimentos/investimentos.php">Investimentos</a>
                      <a href="../dividas/dividas.php">Dívidas</a>
                      <a href="../calculadoras.php">Calculadoras</a>
                      <a href="#">Aprenda</a>
                    </div>
            </div>
            <ul class="opcoesPerfil">
                <li><a href="">Conta</a></li>
                <li><a href="../sair.php">Sair</a></li>
            </ul>
        </div>
    </header>
    <main>
        <section id="principal">
            <div>
                <h2>Adicionar meta</h2>
                <form name="fmrAdicionarMeta" id="fmrAdicionarMeta" >
                    <div>
                        <label for="nomeMeta">Identificador</label>
                        <input type="text" name="nomeMeta" id="nomeMeta" class="input">
                    </div>
                    <div>
                        <label for="valorMeta">Valor</label>
                        <input type="number" name="valorMeta" id="valorMeta" class="mediumInput">
                    </div>
                    <div>
                        <label for="vencimentoMeta">Vencimento</label>
                        <input type="date" name="vencimentoMeta" id="vencimentoMeta" class="mediumInput">
                    </div>
                    <button type="button" name="btnAdicionarMeta" id="btnAdicionarMeta">Adicionar</button>
                </form>
                <a href="metas.php"><button>Cancelar</button></a>
            </div>
        </section>
    </main>
    <footer>
        <div>
            Minhas Finanças© 2023
        </div>
    </footer>
</body>
</html>