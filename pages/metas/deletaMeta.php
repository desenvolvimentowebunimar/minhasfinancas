<?php
session_start();
ini_set("display_errors", 1);
require("../conexao.php");

if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    $data = file_get_contents("php://input");
    $requestData = json_decode($data);

    if (!empty($requestData->id)) {
        $id = $requestData->id;
        $idUsuario = $_SESSION["id"];

        // Verifique se o registro pertence ao usuário logado para evitar exclusões não autorizadas
        $sql = "DELETE FROM financas.metas WHERE id = :id AND fk_user = :id_user";

        $stmt = $conn->prepare($sql);
        $stmt->execute(array(
            ":id" => $id,
            ":id_user" => $idUsuario
        ));

        if ($stmt->rowCount() > 0) {
            $msg=array("codigo" =>1, "texto" => "Meta excluída com sucesso.");
        } else {
            $msg=array("codigo" => 0, "texto" => "Meta não encontrada ou você não tem permissão para excluí-la.");
        }
    } else {
        $msg=array("codigo" => 0, "texto" => "ID da meta não especificado na solicitação.");
    }
} else {
    $msg=array("codigo" => 0, "texto" => "Método de solicitação não suportado.");
}
header('Content-Type: application/json; charset=utf-8');
echo(json_encode($msg));
