document.addEventListener("DOMContentLoaded",()=>{

    function listaConta(){
        let url = "listarMeta.php";
        let resultado = document.querySelector("#listaContas");

        getData(url)
            .then(function (response) {
                // manipula o sucesso da requisição
                let tabela="";
                //monta as linhas da tabela
                response.data.forEach(element => {
                    
                    let vencimentoOriginal = element.vencimento;
                    let partesVencimento = vencimentoOriginal.split("-");
                    let vencimentoFormatado = partesVencimento[2] + "/" + partesVencimento[1] + "/" + partesVencimento[0];

                    let restam = element.valor - element.guardado;

                    tabela+=`<tr>
                        <td>${element.id}</td>
                        <td>${element.nome}</td>
                        <td>${element.guardado.toLocaleString('pt-br', {minimumFractionDigits: 2})}</td>
                        <td>${element.valor.toLocaleString('pt-br', {minimumFractionDigits: 2})}</td>
                        <td>${restam.toLocaleString('pt-br', {minimumFractionDigits: 2})}</td>
                        <td>${vencimentoFormatado}</td>
                        <td><button type="button" class="tableButton" onclick="abreGuardaMeta(${element.id},'${element.nome}')" style="color:blue">Alterar</button></td>
                        <td><button type="button" class="tableButton" onclick="deletaMeta(${element.id})" style="color:red">Deletar</button></td>
                    </tr>`;
                });
                resultado.innerHTML=tabela;
            })
            .catch(function (error) {
                // manipula erros da requisição
                console.error(error);
            })
            .finally(function () {
                // sempre será executado
            });
    }
    listaConta();
})