<?php
session_start();
ini_set("display_errors",1);
require("conexao.php");

$idUsuario = $_SESSION["id"];

$x=file_get_contents('php://input');

$x=json_decode($x);

$nomeItem=$x->nomeItem;
$contaItem=$x->contaItem;
$tipoItem=$x->tipoItem;
$valorItem=$x->valorItem;
if ($valorItem > 0 && $tipoItem == 2) {
    $valorItem *= -1;
}
$vencimentoItem=$x->vencimentoItem;
$vencimentoItem=implode("-",array_reverse(explode("/",$vencimentoItem)));
if ($tipoItem == 1) {
    $pago = 1;
} else {
    $pago = 0;
}

date_default_timezone_set("America/Sao_Paulo");

if ($nomeItem === "" || 
    $contaItem === "" ||
    $valorItem === "" || 
    $vencimentoItem === "") {

    $msg=array("codigo"=>0,"texto"=>"Há campos não preenchidos.");

} else {
    $sql="insert into financas.itens(nome,conta,tipo,valor, vencimento, entrada, pago, fk_user)
    values(:par_nome,:par_conta,:par_tipo,:par_valor,:par_vencimento,:par_entrada, :par_pago, :par_usuario)";
    $stmt = $conn->prepare($sql);
    $dados=array(":par_nome"=>$nomeItem,
            ":par_conta"=>$contaItem,
            ":par_tipo"=>$tipoItem,
            ":par_valor"=>$valorItem,
            ":par_vencimento"=>$vencimentoItem,
            ":par_entrada"=>date("Y-m-d"),
            ":par_pago"=>$pago,
            ":par_usuario"=>$idUsuario
            );
    $result=$stmt->execute($dados);

    if($result){
        $msg=array("codigo"=>1,"texto"=>"Registro inserido com sucesso.");
        
        $sql="UPDATE financas.contas
              SET saldo = saldo + :par_valor
              WHERE nome = :par_conta
                    AND fk_user = :par_usuario";
        $stmt = $conn->prepare($sql);
        $dados=array(":par_conta"=>$contaItem,
                ":par_valor"=>$valorItem,
                ":par_usuario"=>$idUsuario
                );
        $result=$stmt->execute($dados);
    }
    else{
        $msg=array("codigo"=>0,"texto"=>"Erro ao inserir. Verifique os dados.");
    }
}

header('Content-Type: application/json; charset=utf-8');
echo(json_encode($msg));