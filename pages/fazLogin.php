<?php
session_start();
ini_set("display_errors",1);
require("conexao.php");

$x=file_get_contents('php://input');

$x=json_decode($x);

$email=$x->emailLogin;
$senha=$x->passwordLogin;
$query_select = "SELECT id,nome,sobrenome,email
                 FROM financas.users 
                 WHERE email = :par_email and senha=:par_senha";

$stmt = $conn->prepare($query_select);
$stmt->execute(array(
    ":par_email" => $email,
    ":par_senha" => $senha
));
$result=$stmt->fetchAll(PDO::FETCH_OBJ);

if (count($result)>0) {
        $_SESSION["usuario"]=$result[0]->nome;
        $_SESSION["id"]=$result[0]->id;
        $_SESSION["logado"]=true;

        $msg=array("codigo"=>1,"texto"=>"Login OK");
    
} else {
    $msg=array("codigo"=>0,"texto"=>"Usuário/Senha Invalidos");
}

header('Content-Type: application/json; charset=utf-8');
echo(json_encode($msg));