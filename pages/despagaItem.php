<?php
session_start();
ini_set("display_errors", 1);
require("./conexao.php");
$data = file_get_contents("php://input");
$requestData = json_decode($data);

if (!empty($requestData->id)) {
    $id = $requestData->id;
    $idUsuario = $_SESSION["id"];

    $sql = "UPDATE financas.itens
            SET pago = :par_pago
            WHERE id = :id AND fk_user = :id_user";

    $stmt = $conn->prepare($sql);
    $stmt->execute(array(
        ":id" => $id,
        ":id_user" => $idUsuario,
        ":par_pago" => 0
    ));

    if ($stmt->rowCount() > 0) {
        $msg=array("codigo" =>1, "texto" => "Desfeito com sucesso.");
    } else {
        $msg=array("codigo" => 0, "texto" => "Item não encontrado ou você não tem permissão para excluí-la.");
    }
} else {
    $msg=array("codigo" => 0, "texto" => "ID do item não especificado na solicitação.");
}
header('Content-Type: application/json; charset=utf-8');
echo(json_encode($msg));
