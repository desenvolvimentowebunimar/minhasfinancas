<?php
ini_set("display_errors",1);
require("conexao.php");

$x=file_get_contents('php://input');

$x=json_decode($x);

$nome=$x->nomeRegister;
$sobrenome=$x->sobrenomeRegister;
$email=$x->emailRegister;
$senha=$x->passwordRegister;

$query_select = "SELECT count(*) as Total FROM financas.users WHERE email = :par_email";
$stmt = $conn->prepare($query_select);
$stmt->execute(array(
    ":par_email" => $email
));
$result=$stmt->fetchAll(PDO::FETCH_OBJ);

if ($result[0]->Total <= 0) {
    $sql="insert into financas.users(nome,sobrenome,email,senha)
          values(:par_nome,:par_sobrenome,:par_email,:par_senha)";
    $stmt = $conn->prepare($sql);
    $dados=array(":par_nome"=>$nome,
                 ":par_sobrenome"=>$sobrenome,
                 ":par_email"=>$email,
                 ":par_senha"=>$senha
                );
    $result=$stmt->execute($dados);
    
    if($result){
        $msg=array("codigo"=>1,"texto"=>"Registro inserido com sucesso.");
    }
    else{
        $msg=array("codigo"=>0,"texto"=>"Erro ao inserir.");
    }
} else {
    $msg=array("codigo"=>0,"texto"=>"Email já cadastrado.");
}

header('Content-Type: application/json; charset=utf-8');
echo(json_encode($msg));