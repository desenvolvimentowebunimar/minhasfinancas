<?php
session_start();
ini_set("display_errors",1);
require("../conexao.php");

$idUsuario = $_SESSION["id"];

$x=file_get_contents('php://input');

$x=json_decode($x);

$nomeConta=$x->nomeConta;
$bancoConta=$x->bancoConta;
$tipoConta=$x->tipoConta;
$saldoConta=$x->saldoConta;
$vencimentoConta=$x->vencimentoConta;

$query_select = "SELECT count(*) as Total 
                 FROM financas.contas 
                 WHERE nome = :par_nome AND fk_user = :par_usuario";
$stmt = $conn->prepare($query_select);
$stmt->execute(array(
    ":par_nome" => $nomeConta,
    ":par_usuario" => $idUsuario
));
$result=$stmt->fetchAll(PDO::FETCH_OBJ);


if ($nomeConta === "") {
    $msg=array("codigo"=>0,"texto"=>"Identificador da conta não informado.");
} elseif ($result[0]->Total > 0) {
    $msg=array("codigo"=>0,"texto"=>"Identificador de conta já utilizado.");
} else {
    $sql="insert into financas.contas(
                                      nome,
                                      banco,
                                      tipo,
                                      saldo,
                                      dia_vencimento,
                                      fk_user
                                      )
          values(
                 :par_nome,
                 :par_banco,
                 :par_tipo,
                 :par_saldo,
                 :par_vencimento,
                 :par_fk_user
          );";
    $stmt = $conn->prepare($sql);
    $dados=array(":par_nome"=>$nomeConta,
                 ":par_banco"=>$bancoConta,
                 ":par_tipo"=>$tipoConta,
                 ":par_saldo"=>$saldoConta,
                 ":par_vencimento"=>$vencimentoConta,
                 ":par_fk_user"=>$idUsuario ,
                );
    $result=$stmt->execute($dados);

    date_default_timezone_set("America/Sao_Paulo");
    if ($saldoConta > 0 && $tipoConta == "2") {
        $sql="INSERT INTO financas.itens (nome, conta, tipo, valor, pago, vencimento, entrada, fk_user)
              VALUES (:par_nome, :par_conta, :par_tipo, :par_valor, :par_pago, :par_vencimento,:par_entrada, :par_usuario);";
        $stmt=$conn->prepare($sql);
        $dados=array(":par_nome"=>$nomeConta,
                     ":par_conta"=>$nomeConta,
                     ":par_tipo"=>1,
                     ":par_valor"=>$saldoConta,
                     ":par_pago"=>1,
                     ":par_vencimento"=>date("Y-m-d"),
                     ":par_entrada"=>date("Y-m-d"), 
                     ":par_usuario"=>$idUsuario
                    );
        $adcItem=$stmt->execute($dados);
        };

    if($result){
        $msg=array("codigo"=>1,"texto"=>"Registro inserido com sucesso.");
    }
    else{
        $msg=array("codigo"=>0,"texto"=>"Erro ao inserir.");
    }
}
header('Content-Type: application/json; charset=utf-8');
echo(json_encode($msg));