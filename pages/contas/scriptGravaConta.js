document.addEventListener("DOMContentLoaded",()=>{
    function envia(){
        let url="gravaConta.php";
        let form = document.querySelector("#fmrAdicionarConta");
        postForm(url,form).then(res=>{
            if(res.data.codigo=="1"){
                Swal.fire({
                    title: 'Sucesso',
                    text: res.data.texto,
                    icon: 'success',
                    showConfirmButton: false
                    });
                    setTimeout(() => {
                        window.location.href = 'contas.php';
                    }, 2000);
            }
            else{
                Swal.fire({
                    title: 'Erro',
                    text: res.data.texto,
                    icon: 'error',
                    confirmButtonText: 'OK',
                    confirmButtonColor: '#111011'
                    })
            }
        })
    }

    document.querySelector("#btnAdicionarConta").addEventListener("click",function(){
        envia();
    });

})