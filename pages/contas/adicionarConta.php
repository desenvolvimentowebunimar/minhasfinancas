<?php 
session_start();
if(!$_SESSION["logado"]){
    header("Location: ../login.html");
    exit;
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Minhas Finanças: Contas</title>
    <link rel="stylesheet" href="../../styles/stylePrincipal.css">
    <link rel="stylesheet" href="../../styles/styleAdicionarItem.css">
    <link rel="stylesheet" href="../../styles/styleLoginRegister.css">
    <link rel="stylesheet" href="../../style.css">
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.32/dist/sweetalert2.all.min.js"></script>
    <link href=" https://cdn.jsdelivr.net/npm/sweetalert2@11.7.32/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="../../scripts.js"></script>
    <script src="../../lib.js"></script>
    <script src="./scriptGravaConta.js"></script>
</head>
<body>
    <header>
        <div id="divisao-topo">
            <div class="menuEsquerda">
                <div class="menuButton" onclick="myFunction(this)">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                </div>
                <a href="../principal.php" class="inicio">Minhas Finanças</a>
                <div class="subMenuItens">
                    <a href="contas.php">Contas</a>
                    <a href="../metas/metas.php">Metas</a>
                    <a href="../investimentos/investimentos.php">Investimentos</a>
                    <a href="../dividas/dividas.php">Dívidas</a>
                    <a href="../calculadoras.php">Calculadoras</a>
                </div>
            </div>
            <ul class="opcoesPerfil">
                <li><a href="../sair.php">Sair</a></li>
            </ul>
        </div>
    </header>
    <main>
        <section id="principal">
            <div>
                <h2>Adicionar conta</h2>
                <form name="fmrAdicionarConta" id="fmrAdicionarConta">
                    <div>
                        <label for="nomeConta">Identificador</label>
                        <input type="text" name="nomeConta" id="nomeConta" class="input" required>
                    </div>
                    <div>
                        <label for="bancoConta">Banco</label>
                        <input type="text" name="bancoConta" id="bancoConta" class="input">
                    </div>
                    <div>
                        <label for="tipoConta">Tipo</label>
                        <div>
                            <input type="radio" name="tipoConta" id="tipoConta" value="1">Cartão de crédito
                            <input type="radio" name="tipoConta" id="tipoConta" value="2" checked>Bancária
                        </div>
                    </div>
                    <div>
                        <label for="saldoConta">Valor inicial (Opcional)</label>
                        <input type="number" name="saldoConta" id="saldoConta" class="mediumInput" value="0.00">
                    </div>
                    <div>
                        <label for="vencimentoConta">Dia do vencimento (Opcional)</label>
                        <input type="number" name="vencimentoConta" id="vencimentoConta" class="mediumInput" value="0">
                    </div>
                    <button type="button" id="btnAdicionarConta">Adicionar</button>
                </form>
                <a href="contas.php"><button>Cancelar</button></a>
            </div>
        </section>
    </main>
    <footer>
        <div>
            Minhas Finanças© 2023
        </div>
    </footer>
</body>
</html>