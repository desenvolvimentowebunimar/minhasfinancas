document.addEventListener("DOMContentLoaded",()=>{
    function listaConta(){
        let url = "listarConta.php";
        let resultado = document.querySelector("#listaContas");

        getData(url)
            .then(function (response) {
                // manipula o sucesso da requisição
                let tabela="";
                //monta as linhas da tabela
                response.data.forEach(element => {
                    tabela+=`<tr data-id="${element.id}">
                        <td>${element.id}</td>
                        <td>${element.nome}</td>
                        <td>${element.banco}</td>
                        <td>${element.tipo}</td>
                        <td>${element.saldo.toLocaleString('pt-br', {minimumFractionDigits: 2})}</td>
                        <td>${element.dia_vencimento}</td>
                        <td><button type="button" class="tableButton" onclick="deletaItem(${element.id})" style="color:red">Deletar</button></td>
                    </tr>`;
                });
                resultado.innerHTML=tabela;
            })
            .catch(function (error) {
                // manipula erros da requisição
                console.error(error);
            })
            .finally(function () {
                // sempre será executado
            });
    }
    
    listaConta();

    });