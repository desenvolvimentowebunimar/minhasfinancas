async function postForm(url,form){
    let data=Object.fromEntries(new FormData(form));
    const response = await axios.post(url,data);
    return response;
}

async function getData(url){
    const response = await axios.get(url);
    return response;
}

async function deleteRecord(url, data){
    const response = await axios.delete(url, data);
    return response;
}

async function modifyRecord(url, data){
    const response = await axios.patch(url, data);
    return response;
}