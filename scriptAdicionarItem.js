document.addEventListener("DOMContentLoaded",()=>{
    function envia(){
        let url="gravaItem.php";
        let form = document.querySelector("#fmrAdicionarItem");
        postForm(url,form).then(res=>{
            if(res.data.codigo=="1"){
                Swal.fire({
                    title: 'Sucesso',
                    text: res.data.texto,
                    icon: 'success',
                    confirmButtonText: 'OK',
                    showConfirmButton: false
                    });
                    setTimeout(() => {
                        window.location.href = './principal.php';
                    }, 2000);
            }
            else{
                Swal.fire({
                    title: 'Erro',
                    text: res.data.texto,
                    icon: 'error',
                    confirmButtonText: 'OK'
                    })
            }
        })
    }

    function getContas(){
        //faz a requisicao para o backend retornar os cursos
        getData('contas/listarConta.php')
            .then((res)=>{
                let opt="<option value='' selected>Selecione uma conta</option>";
                res.data.forEach(element=>{
                    opt+=`<option value="${element.nome}">${element.nome}</option>`;
                });
                document.querySelector("#contaItem").innerHTML=opt;
            
            });
    }
    
    document.querySelector("#btnAdicionarItem").addEventListener("click",function(){
        
        envia();
    });

    getContas();

});