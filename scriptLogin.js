document.addEventListener("DOMContentLoaded",()=>{
    function entrar(){
        let url="fazLogin.php";
        let form = document.querySelector("#fmrLogin");
        postForm(url,form).then(res=>{
            if(res.data.codigo=="1"){
                window.location.href = './principal.php';
            }
            else{
                Swal.fire({
                    title: 'Erro',
                    text: res.data.texto,
                    icon: 'error',
                    confirmButtonText: 'OK',
                    confirmButtonColor: '#111011'
                    })
                document.querySelector('#passwordLogin').value = '';
            }
        })
    }
    
    document.querySelector("#btnLogin").addEventListener("click",function(){
        entrar();
    });

});